module github.com/renproject/lightnode

go 1.13

require (
	github.com/btcsuite/btcd v0.20.1-beta
	github.com/btcsuite/btcutil v1.0.1
	github.com/ethereum/go-ethereum v1.9.5
	github.com/evalphobia/logrus_sentry v0.8.2
	github.com/go-redis/redis/v7 v7.0.0-beta.5
	github.com/google/go-cmp v0.4.0
	github.com/google/uuid v1.1.1 // indirect
	github.com/lib/pq v1.2.0
	github.com/mattn/go-sqlite3 v1.11.0
	github.com/onsi/ginkgo v1.11.0
	github.com/onsi/gomega v1.8.1
	github.com/renproject/darknode v0.5.3-0.20200518024655-933174702d20
	github.com/renproject/kv v1.1.2
	github.com/renproject/mercury v0.3.13-0.20200513075747-7a2ec34c238e
	github.com/renproject/phi v0.1.0
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
)
