# `🚀 lightnode`

![](https://github.com/renproject/lightnode/workflows/go/badge.svg)
[![Coverage Status](https://coveralls.io/repos/github/renproject/lightnode/badge.svg?branch=master)](https://coveralls.io/github/renproject/lightnode?branch=master)

A node used for querying Darknodes using JSON-RPC 2.0 interfaces. Featuring query caching (for performance) as well as retrying for failed requests (for reliability).

Built with ❤ by Ren.
