package testutils

import "github.com/renproject/darknode/jsonrpc"

var QueryRequests = []string{
	jsonrpc.MethodQueryBlock,
	jsonrpc.MethodQueryBlocks,
	jsonrpc.MethodQueryNumPeers,
	jsonrpc.MethodQueryPeers,
	jsonrpc.MethodQueryStat,
	// jsonrpc.MethodQueryEpoch,
	jsonrpc.MethodQueryTx,
}
