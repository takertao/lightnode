## 0.1.7
- Update confirmations to be the same as Darknodes

## 0.1.6

- Add support for Mainnet network
- Update to go v1.13

## 0.1.5

- Update Lightnode to use the Darknode JSON-RPC server
- Add support for the QueryShards method
- Ensure bootstrap Darknodes are not removed from storage
